Python Flask订餐系统
=====================
## 说明文档
### 安装所有依赖包
    pip -r install requirements.txt
### 修改域名
    修改config中的domain
    修改app.js中的domain 
  
## 商铺信息注册
    https://pay.weixin.qq.com/apply/applyment4normal/detail

### 启动 
    set ops_config=local && python manage.py runserver

### flask-sqlacodegen
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --outfile "common/models/model.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables user --outfile "common/models/user.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables app_access_log --outfile "common/models/log/AppAccessLog.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables app_error_log --outfile "common/models/log/AppErrorLog.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables member --outfile "common/models/member/Member.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables oauth_member_bind --outfile "common/models/member/OauthMemberBind.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables food --outfile "common/models/food/food.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables food_cat --outfile "common/models/food/food_cat.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables food_sale_change_log --outfile "common/models/food/food_sale_change_log.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables food_stock_change_log --outfile "common/models/food/food_stock_change_log.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables wx_share_history --outfile "common/models/WxShareHistory/wx_share_history.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables member_cart --outfile "common/models/member/member_cart.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables member_address --outfile "common/models/member/member_address.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables pay_order --outfile "common/models/pay/pay_order.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables pay_order_item --outfile "common/models/pay/pay_order_item.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables pay_order_callback_data --outfile "common/models/pay/pay_order_callback_data.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables queue_list --outfile "common/models/queue/queue_list.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables oauth_access_token --outfile "common/models/pay/oauth_access_token.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables member_comments --outfile "common/models/member/member_comments.py" --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables member_address --outfile "common/models/member/member_address.py"  --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables stat_daily_food --outfile "common/models/stat/stat_daily_food.py" --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables stat_daily_member --outfile "common/models/stat/stat_daily_member.py" --flask
    flask-sqlacodegen "mysql://root:0000@127.0.0.1/food_db" --tables stat_daily_site --outfile "common/models/stat/stat_daily_site.py" --flask

### 功能列表
- 管理员后台账号模块
   > - 登录退出功能  
   > - 对所有请求页面判断登录状态 - 拦截器  
   > - 编辑和修改登录人信息  
   > - 列表和详情展示所有用户信息  
   > - 添加和编辑  
   > - 搜索和分页  
   > - 删除和恢复  
   - 功能优化
   > 1. 删除账号即时退出登录问题  
     请求拦截器  
     登录判断状态  
   > 2. 浏览器缓存即时刷新问题  
     版本号变化更新
   > 3. 访问记录日志
   > 4. 错误处理日志

- 小程序登录  
    > 小程序登录流程官方文档 https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/login.html  
    - API
    > 1.http://172.25.0.247:8999/api/member/check-reg
    > 2.http://172.25.0.247:8999/api/member/login
    
- 会员模块
    - 功能  
      1. 会员列表  
      2. 编辑会员  
      3. 删除会员  
      4. 恢复会员  
      5. 会员详情  

- 菜品模块
    > - 菜品相关数据表设计  
    - 功能    
       1. 菜品分类列表  
       2. 添加/编辑/删除/恢复菜品分类
       3. 菜品列表
       4. 添加/编辑/删除/恢复菜品
       5. 菜品库存变更历史
    
    - 三个插件  
       1. ueditor  
       2. tagsinput  
       3. select2  
    - 无刷新上传  
       1. iframe
   
- 小程序菜品模块
    > - 加入购物车、分享记录相关表设计
    - 功能
      1. 加入购物车
      2. 菜品列表
      3. 菜品详情
    
- 订单模块开发
    > - 订单相关数据表设计
    - 功能
      1. 会员在线下单
      2. 在线支付
      3. 订单列表、取消订单、我要付款
      4. 库存释放
      5. 收货功能  
      6. 会员评价、评价列表
      7. 收货地址管理
      8. 模板消息管理
      
- 会员中心模块开发
    > - 收货地址
    > - 我的评论
    
- 管理员后台财务模块开发
    - 订单管理
    > - 订单列表
    > - 订单详情
    - 财务流水
    
- 统计模块开发
    > - 相关统计表设计
    - 功能
        0. 仪表盘
        1. 财务日统计
        2. 商品销售日统计
        3. 会员消费统计
        4. 会员分享统计
        
        
        
