# -*- coding: utf-8 -*-
import time
from flask import current_app


class UrlManager(object):
    def __init__(self):
        pass

    @staticmethod
    def buildUrl(path):
        return path

    @staticmethod
    def buildStaticUrl(path):
        release_version = current_app.config.get('RELEASE_VERSION')
        ver = release_version if release_version else str(int(time.time()))
        path = "/static" + path + "?ver=" + ver
        return UrlManager.buildUrl(path)

    @staticmethod
    def buildImageUrl(path):
        app = current_app._get_current_object()
        url = app.config['APP']['domain'] + app.config['UPLOAD']['prefix_url'] + path
        return url
