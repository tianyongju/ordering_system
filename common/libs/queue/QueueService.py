# -*- coding: utf-8 -*-

"""
@Datetime: 2019/2/20
@Author: Zhang Yafei
"""
from application import app, db
from common.models.queue.queue_list import QueueList
from common.libs.helper import getCurrentDate


class QueueService(object):
    @staticmethod
    def add_queue(queue_name, data=None):
        model_queue = QueueList()
        model_queue.queue_name = queue_name
        if data:
            model_queue.data = data

        model_queue.created_time = model_queue.updated_time = getCurrentDate()
        db.session.add(model_queue)
        db.session.commit()
        return True
