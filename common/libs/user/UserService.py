# -*- coding: utf-8 -*-

"""
@Datetime: 2019/1/31
@Author: Zhang Yafei
"""
import hashlib, base64, random, string


class UserService(object):

    @staticmethod
    def genAuthCode(userinfo):
        m = hashlib.md5()
        str = '{}-{}-{}-{}'.format(userinfo.uid, userinfo.login_name, userinfo.login_pwd, userinfo.login_salt)
        m.update(str.encode('utf-8'))
        return m.hexdigest()

    @staticmethod
    def genePwd(pwd, salt):
        m = hashlib.md5()
        str = '{}-{}'.format(base64.encodebytes(pwd.encode('utf-8')), salt)
        m.update(str.encode('utf-8'))
        return m.hexdigest()

    @staticmethod
    def geneSalt(length=16):
        key_list = [random.choice((string.ascii_letters + string.digits)) for i in range(length)]
        return (''.join(key_list))