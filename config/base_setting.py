# -*- coding: utf-8 -*-

"""
@Datetime: 2019/1/29
@Author: Zhang Yafei
"""
SERVER_PORT = 8999
DEBUG = True
JSON_AS_ASCII = False  # json数据中文显示正确

AUTH_COOKIE_NAME = 'mooc_food'

## 过滤url
IGNORE_URLS = [
    '^/user/login',
]

IGNORE_CHECK_LOGIN_URLS = [
    '^/static',
    '^/favicon.ico'
]

API_IGNORE_URLS = [
    "^/api"
]

PAGE_SIZE = 15
PAGE_DISPLAY = 10

STATUS_MAPPING = {
    '1': '正常',
    '0': '已删除',
}

MINA_APP = {
    'appid': 'wx49bec3712cb29bf5',
    'appkey': '6bbc5f0312db5bf8349cadc081b06a46',
    'paykey': '',
    'mch_id': '',
    'callback_url': '/api/order/callback',
}

UPLOAD = {
    'ext': ['jpg', 'gif', 'bmp', 'jpeg', 'png'],
    'prefix_path': '/web/static/upload/',
    'prefix_url': '/static/upload/',
}
APP = {
    'domain': 'http://172.25.0.175:8999'
}

PAY_STATUS_MAPPING = {
    "1": "已支付",
    "-8": "待支付",
    "0": "已关闭"
}

PAY_STATUS_DISPLAY_MAPPING = {
    '0': '订单关闭',
    '1': '支付成功',
    '-8': '待支付',
    '-7': '待发货',
    '-6': '待确认',
    '-5': '待评价',
}
