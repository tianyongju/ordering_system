# -*- coding: utf-8 -*-

"""
@Datetime: 2019/1/29
@Author: Zhang Yafei
"""
import www
from application import app, manager, db
from flask_script import Server
from jobs.launcher import runJob
from flask_migrate import Migrate, MigrateCommand

# python manage.py runserver
manager.add_command('runserver', Server(host='0.0.0.0', port=app.config['SERVER_PORT'], use_debugger=True, use_reloader=True))

# database migrate comand
"""
    python manage.py db init
    python manage.py db migrate
    python manage.py db upgrade
"""
Migrate(app, db)
manager.add_command('db', MigrateCommand)

# job entrance
manager.add_command('runjob', runJob())


def main():
    manager.run()


if __name__ == '__main__':
    try:
        import sys
        sys.exit(main())
    except Exception as e:
        import traceback
        traceback.print_exc()