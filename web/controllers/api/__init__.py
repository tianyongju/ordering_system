# -*- coding: utf-8 -*-

"""
@Datetime: 2019/2/14
@Author: Zhang Yafei
"""
from flask import Blueprint
route_api = Blueprint('api_page', __name__)
from web.controllers.api.Member import *
from web.controllers.api.food import *
from web.controllers.api.Cart import *
from web.controllers.api.order import *
from web.controllers.api.my import *
from web.controllers.api.address import *


@route_api.route('/')
def index():
    return 'Mina Api V1.0~~'
