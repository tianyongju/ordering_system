# -*- coding: utf-8 -*-

"""
@Datetime: 2019/2/17
@Author: Zhang Yafei
"""
from sqlalchemy import or_
from web.controllers.api import route_api
from flask import jsonify, request, g
from application import app, db
from common.models.food.food import Food
from common.models.food.food_cat import FoodCat
from common.models.member.member_cart import MemberCart
from common.libs.UrlManager import UrlManager
from common.libs.member.MemberService import MemberService


@route_api.route('/food/index')
def food_index():
    resp = {'code': 200, 'msg': '操作成功', 'data': {}}
    cat_list = FoodCat.query.filter_by(status=1).order_by(FoodCat.weight.desc(), FoodCat.id.desc()).all()
    data_cat_list = []
    data_cat_list.append({
        'id': 0,
        'name': '全部',
    })
    if cat_list:
        data_cat_list.extend(list(map(lambda item: {'id': item.id, 'name': item.name}, cat_list)))
    resp['data']['cat_list'] = data_cat_list
    food_list = Food.query.filter_by(status=1).order_by(Food.total_count.desc(), Food.id.desc()).limit(3).all()
    data_food_list = []
    if food_list:
        data_food_list.extend(list(map(lambda item: {'id': item.id, 'pic_url': UrlManager.buildImageUrl(item.main_image)}, food_list)))
    resp['data']['banner_list'] = data_food_list
    return jsonify(resp)


@route_api.route('/food/search')
def food_search():
    resp = {'code': 200, 'msg': '操作成功', 'data': {}}
    req = request.values
    cat_id = int(req['cat_id']) if 'cat_id' in req else 0
    mix_kw = str(req['mix_kw']) if 'mix_kw' in req else ''
    p = int(req['p']) if 'p' in req else 0
    if p < 1:
        p = 1
    query = Food.query.filter_by(status=1)

    page_size = 10
    offset = (p-1) * page_size
    if mix_kw:
        rule = or_(Food.name.ilike('%{0}%'.format(mix_kw)), Food.tags.ilike('%{0}%'.format(mix_kw)))
        query = query.filter(rule)

    if cat_id > 0:
        query = query.filter(Food.cat_id == cat_id)

    food_list = query.order_by(Food.total_count.desc(), Food.id.desc()).offset(offset).limit(page_size).all()

    # data_food_list = list(map(lambda item: {'id': item.id, 'name': item.name, 'price': str(item.price), 'min_price': str(item.price), 'pic_url':UrlManager.buildImageUrl(item.main_image)}, food_list)) if food_list else []
    data_food_list = [list(map(lambda item: {'id': item.id, 'name': item.name, 'price': str(item.price), 'min_price': str(item.price), 'pic_url':UrlManager.buildImageUrl(item.main_image)}, food_list)), []][food_list is None]

    resp['data']['list'] = data_food_list
    resp['data']['has_more'] = 0 if len(data_food_list) < page_size else 1
    return jsonify(resp)


@route_api.route('/food/info')
def food_info():
    resp = {'code': 200, 'msg': '操作成功', 'data': {}}
    req = request.values
    id  = int(req['id']) if 'id' in req else 0
    food_info = Food.query.filter_by(id=id).first()
    if not food_info or not food_info.status:
        resp['code'] = -1
        resp['msg'] = '美食已下架~~'
        return jsonify(resp)

    member_info  = g.member_info
    cart_number = 0
    if member_info:
        cart_number = MemberCart.query.filter_by(member_id=member_info.id).count()

    resp['data']['info'] = {
        'id': food_info.id,
        'name': food_info.name,
        'summary': food_info.summary,
        'total_count': food_info.total_count,
        'comment_count': food_info.comment_count,
        'main_image': UrlManager.buildImageUrl(food_info.main_image),
        'price': str(food_info.price),
        'stock': food_info.stock,
        'pics': [UrlManager.buildImageUrl(food_info.main_image)],

    }
    resp['data']['cart_number'] = cart_number

    return jsonify(resp)


