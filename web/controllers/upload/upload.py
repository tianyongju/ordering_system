# -*- coding: utf-8 -*-

"""
@Datetime: 2019/2/16
@Author: Zhang Yafei
"""
import json
import re
from application import app
from flask import Blueprint, request, jsonify
from common.libs.UploadService import UploadService
from common.libs.UrlManager import UrlManager
from common.models.Image import Image


route_upload = Blueprint('upload_page', __name__)


@route_upload.route('/ueditor', methods=['GET', 'POST'])
def ueditor():
    req = request.values
    action = req['action'] if 'action' in req else ''
    if action == 'config':
        root_path = app.root_path
        config_path = '{0}/web/static/plugins/ueditor/upload_config.json'.format(root_path)
        print(config_path)
        with open(config_path, encoding='utf-8') as fp:
            try:
                config_data = json.loads(re.sub(r'\/\*.*\*\/', '', fp.read()))
            except Exception as e:
                import traceback
                traceback.print_exc()
                config_data = {}
        return jsonify(config_data)

    if action == 'uploadimage':
        return uploadImage()

    if action == 'listimage':
        return listImage()

    return 'upload'


@route_upload.route('/pic', methods=['GET','POST'])
def upload_pic():
    file_target = request.files
    upfile = file_target['pic'] if 'pic' in file_target else None
    callback_target = 'window.parent.upload'
    if upfile is None:
        return '<script type="text/javascript">{0}.error("{1}")</script>'.format(callback_target,'上传失败')

    ret = UploadService.uploadbyfile(upfile)
    if ret['code'] != 200:
        return '<script type="text/javascript">{0}.error("{1}")</script>'.format(callback_target,'上传失败')

    return '<script type="text/javascript">{0}.success("{1}")</script>'.format(callback_target, ret['data']['file_key'])


def uploadImage():
    resp = {'state': 'SUCCESS', 'url': '', 'title': '', 'original': ''}
    file_target = request.files
    upfile = file_target['upfile'] if 'upfile' in file_target else None
    if upfile is None:
        resp['state'] = '上传失败'
        return jsonify(resp)
    ret = UploadService.uploadbyfile(upfile)
    if ret['code'] != 200:
        resp['state'] = '上传失败：' + ret['msg']
        return jsonify(resp)

    resp['url'] = UrlManager.buildImageUrl(ret['data']['file_key'])
    return jsonify(resp)


def listImage():
    resp = {'state': 'SUCCESS', 'list': [], 'start': 0, 'total': 0}
    req = request.values
    start = int(req['start']) if 'start' in req else 0
    page_size = int(req['size']) if 'size' in req else 20

    # 方式一
    query = Image.query
    if start > 0:
        query = query.filter(Image.id < start)

    query_list = query.order_by(Image.id.desc()).limit(page_size).all()
    images = []
    if query_list:
        for item in query_list:
            images.append({'url': UrlManager.buildImageUrl(item.file_key)})
            start = item.id
    resp['list'] = images
    resp['start'] = start
    resp['total'] = len(images)
    return jsonify(resp)

    # 方式二
    # page = start if start > 0 else 1
    # offset = (page - 1) * page_size
    # query_list = Image.query.order_by(Image.id.desc()).offset(offset).limit(page_size).all()
    # images = []
    # if query_list:
    #     for item in query_list:
    #         images.append({'url': UrlManager.buildImageUrl(item.file_key)})
    # resp['list'] = images
    # resp['start'] = page + 1
    # resp['total'] = len(images)
    # return jsonify(resp)