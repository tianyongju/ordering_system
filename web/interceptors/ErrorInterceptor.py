# -*- coding: utf-8 -*-

"""
@Datetime: 2019/2/9
@Author: Zhang Yafei
"""
from application import app
from common.libs.helper import ops_render
from common.libs.LogService import LogService


@app.errorhandler(404)
def error_404(e):
    LogService.addErrorLog(str(e))
    return ops_render('error/error.html', {'status':404, 'msg':'很抱歉，您访问的页面不存在'})